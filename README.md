# SKYBET TECH TEST SOLUTION

## Technologies
- React
- Redux
- React Router
- ECMAScript 6
- Component testing using Enzyme and Jest
- Code Coverage
- Webpack
- SASS
- Babel
- Express

## Requirements
Node.js

## How to install
- Run `npm install`

## Starting the app
- Start backend `npm run server`
- Open [http://localhost:8181/users](http://localhost:8181)
- Start the app `npm start`
- Open [http://localhost:8080](http://localhost:8080)

## Available Commands

- `npm start` - start the dev server
- `npm run server` - start the backend
- `npm clean` - delete the dist folder
- `npm run lint` - execute an eslint check
- `npm test` - run all tests
- `npm run test:watch` - run all tests in watch mode
- `npm run coverage` - generate code coverage report in the `coverage` folder
